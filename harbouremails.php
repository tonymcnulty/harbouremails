<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL);

set_include_path(get_include_path() . PATH_SEPARATOR . '/usr/share/harbouremails/lib');

require_once 'Zend/Mime.php';
require_once 'Zend/Mime/Decode.php';
require_once 'Zend/Mail/Part.php';
require_once 'Zend/Mail/Message.php';
require_once 'MailPoster.php';
require_once 'MailLogger.php';

$content = file_get_contents("php://stdin");

$tmp_filename = "/tmp/hbr_" . microtime(true) . ".msg";
file_put_contents($tmp_filename, $content);

$logger = new MailLogger($tmp_filename);

$logger->log("New email received.");

$email = new Zend_Mail_Message(array('raw' => $content));

$headers = $email->getHeaders();
if (count($headers) == 0) {
  $logger->log("Could not parse email format. Quitting.");
  exit;
}
if (!isset($headers['envelope-to'])) {
  $logger->log("No recipients specified in the envelope. Quitting.");
  exit;
}

$logger->log("Recipients found to deliver to: " . $headers['envelope-to']);

$domains = array();
$recipients = explode(', ', $headers['envelope-to']);
foreach ($recipients as $recipient) {
  list($user, $domain) = explode('@', $recipient);
  $domains[$domain] = 1;
}
$domains = array_keys($domains);

$logger->log("Unique domains found to deliver to: " . implode(", ", $domains));

$mailposter = new MailPoster();
$mailposter->setContent($content);
foreach ($domains as $domain) {
  $results = $mailposter->post($domain);
  $logger->log($domain . ": POST " . $results);
}
