<?php

class MailPoster {
  var $_ch = false;

  function __construct() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Harbour Integrator/0.1');

    $this->_ch = $ch;
  }

  public function setContent($content) {
    curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $content);
  }

  public function post($domain) {
    $url = 'http://' . $domain . '/api/incomingmail/';

    curl_setopt($this->_ch, CURLOPT_URL, $url);
    $output = curl_exec($this->_ch);
    $info = curl_getinfo($this->_ch);

    $results = $info['url'] . ' ' . $info['http_code'];
    if ($output) {
      $results .= ': ' . str_replace("\n", '', $output);
    }

    return $results;
  }

  function __destruct() {
    curl_close($this->_ch);
  }
}
