<?php

class MailLogger {

  var $_logfile = '/var/log/harbouremails.log';
  var $_filename = '';

  function __construct($filename) {
    $this->_filename = $filename;
  }

  public function log($message) {
    $message = sprintf("%s - %s: %s", $this->_date(), $this->_filename, $message);

    $this->_write($message);
  }

  private function _date() {
    return date("Y-m-d H:i:s");
  }

  private function _write($message) {
    $fh = fopen($this->_logfile, 'a');
    fwrite($fh, $message . "\n");
    fclose($fh);
  }
}
